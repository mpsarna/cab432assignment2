const twitterget = require('./twitterget.js');
const ANALYZER = require('./sentimentAnalyzer.js');
const AZURESQL = require('./azureSql.js');

// The router class handles the routes for the server
function REST_ROUTER(router) {
	let self = this;
	self.handleRoutes(router);
}

// Handles the routes
REST_ROUTER.prototype.handleRoutes = function(router) {

    // The index page
    router.get("/", function (req, res) {
        res.render("index", {
            resultTitle: 'hidden'
        });
    });

    // Shows the results of tweet search
    router.get("/search", function (req, res) {
        let keyword = req.query.keyword;
        let numTweets = req.query.numTweets;

        console.log("Received Input From Website");
        console.log(keyword);
        console.log(numTweets);

        // Gets a list of Tweets
        twitterget.gettwits(keyword, numTweets, function(newTweets){
            if (newTweets === 'error') {
                renderResult(res, 'Error in getting Tweets!');
            }
            else {
                const numNewTweets = newTweets.length;

                let azureSql = new AZURESQL();

                // When the new tweets are not enough, gets some cached tweets
                if (numNewTweets < numTweets) {
                    azureSql.queryTweets(keyword, numTweets - numNewTweets)
                        .then((cachedTweets) => {
                            console.log("Getting cached tweets");

                            // Deals with the case when no tweet is cached for this keyword
                            if (cachedTweets === "no cache") {
                                // Deals with the case when no new tweet is returned and no tweet is cached for this keyword
                                if (numNewTweets == 0) {
                                    renderResult(res, "No tweet can be found!");
                                }
                                else {
                                    // Queries the classifier model for sentiment analysis from Azure Cosmos DB
                                    azureSql.queryContainer()
                                        .then((modelJson) => {
                                            let analyzer = new ANALYZER(modelJson);

                                            let sentiments = analyzer.analyzeTweets(newTweets);

                                            // Makes a map with tweet's content and sentiment
                                            let results = createResult(newTweets, sentiments);

                                            console.log("Data Received from Twitter");

                                            cacheTweets(azureSql, keyword, newTweets);

                                            //sending results to webpage
                                            renderResult(res, results);
                                        })
                                        .catch((error) => {
                                            renderResult(res, error);
                                        });
                                }
                            }
                            // When there are cached tweets for the keyword
                            else {
                                let tweets;

                                if (numNewTweets == 0) {
                                    tweets = cachedTweets;
                                }
                                else {
                                    cacheTweets(azureSql, keyword, newTweets);
                                    tweets = newTweets.concat(cachedTweets);
                                }

                                // Queries the classifier model for sentiment analysis from Azure Cosmos DB
                                azureSql.queryContainer()
                                    .then((modelJson) => {
                                        let analyzer = new ANALYZER(modelJson);

                                        let sentiments = analyzer.analyzeTweets(tweets);

                                        // Makes a map with tweet's content and sentiment
                                        let results = createResult(tweets, sentiments);

                                        console.log("Data Received from Twitter");

                                        //sending results to webpage
                                        renderResult(res, results);
                                    })
                                    .catch((error) => {
                                        renderResult(res, error);
                                    });
                            }
                        });
                }
                // When the number of new tweets is enough
                else {
                    // Queries the classifier model for sentiment analysis from Azure Cosmos DB
                    azureSql.queryContainer()
                        .then((modelJson) => {
                            let analyzer = new ANALYZER(modelJson);

                            let sentiments = analyzer.analyzeTweets(newTweets);

                            // Makes a map with tweet's content and sentiment
                            let results = createResult(newTweets, sentiments);

                            console.log("Data Received from Twitter");

                            cacheTweets(azureSql, keyword, newTweets);

                            //sending results to webpage
                            renderResult(res, results);
                        })
                        .catch((error) => {
                            renderResult(res, error);
                        });
                }
            }
        });
    });
}

// Renders the results to page
function renderResult(res, results) {
    res.render("index", {
        result: results
    });
}

// Creates a result map with tweet text and sentiment
function createResult(tweets, sentiments) {
    let results = [];

    for (let i in sentiments) {
        let result = {};
        result['text'] = tweets[i];
        result['sentiment'] = sentiments[i];

        results.push(result);
    }

    return results;
}

// Stores new tweets to DB
function cacheTweets(azureSql, keyword, tweets) {
    for (let i in tweets) {
        let tweetJson = {"text":tweets[i]};
        azureSql.addTweet(keyword, tweetJson);
    }
}


module.exports = REST_ROUTER;