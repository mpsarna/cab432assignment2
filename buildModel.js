const fsSync = require('fs-sync');
const bayes = require('bayes');


/*
* A set of methods used to train and store a Naive Bayes classifier model using a prepared file with tweets
* as training data.
*/

function trainModel(trainingData) {
    let classifier = bayes();

    for (let i = 0; i < trainingData.length; i++) {
        classifier.learn(trainingData[i][0], trainingData[i][1])
    }

    return classifier;
}

function loadTrainingData() {
    const rawData = fsSync.read('trainingTweets.txt').split('\n');

    let data = [];

    for (let i = 0; i < rawData.length; i++) {
        let parts = rawData[i].split('\t\t');
        let tweet = parts[0];
        let sentiment;
        let rawSentiment = parts[1];

        if (rawSentiment == 0) {
            sentiment = 'negative';
        }
        else {
            sentiment = 'positive';
        }

        data.push([tweet, sentiment]);
    }

    return data;
}

function storeModel(model) {
    let path = 'models/nbModel.json';
    let modelJson = model.toJson();
    fsSync.write(path, modelJson);
}


let data = loadTrainingData();
let model = trainModel(data);
storeModel(model);