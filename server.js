const express = require("express");
const bodyParser = require('body-parser');
const REST_ROUTER = require("./restRouter.js");

const app  = express();

const port = 3000;

// The REST server handles the basic configurations
function REST() {
	let self = this;
    self.configure();
}

// Configures the server
REST.prototype.configure = function() {
	let self = this;

	let router = express.Router();
	app.use('', router);

    app.use(express.static(__dirname + '/views/'));
	app.set('view engine', 'hbs');

	app.use(bodyParser.urlencoded({extended : true}));

	//// Setup Socket Connection
	// const server = http.createServer(app);
	// const io = socketio.listen(server);

	new REST_ROUTER(router);
	self.startServer();
}

// Starts the server
REST.prototype.startServer = function() {
	app.listen(port, function() {
		console.log('Express app listening at http://localhost:'+port+'/');
	});
}



new REST;

