const CosmosClient = require("@azure/cosmos").CosmosClient;


/*
* The class provides an interface that to query object from Azure Cosmos DB
*/
function AZURESQL() {
    const endpoint = "https://cab432a2.documents.azure.com:443/";
    const masterKey = "1028BgNUgycCFifVFjOKqk6QHE218F37aFe2ffTEaGKznVVDh6mIzDCNBXcvCoVXggsoHoTpq8HVVoGRYr1hIQ==";

    this.client = new CosmosClient({ endpoint: endpoint, auth: { masterKey: masterKey } });
    this.modelDatabaseId = "models";
    this.modelContainerId = "nb";
}

AZURESQL.prototype.queryContainer = async function () {
    console.log(`Querying container: ${this.modelContainerId}`);

    // query to return all children in a family
    const querySpec = {
        query: "SELECT * FROM c"
    };

    // Gets object from the db
    const { result: results } = await this.client.database(this.modelDatabaseId).container(this.modelContainerId)
        .items.query(querySpec).toArray();

    // Extracts the model from the first JSON object from the query result
    let result = JSON.stringify(results[0]);

    return result;
};

AZURESQL.prototype.addTweet = async function (keyword, tweetJson) {
    const databaseId = "tweets";
    const containerId = keyword;

    await this.client.database(databaseId).containers.createIfNotExists({ id: containerId });
    await this.client.database(databaseId).container(keyword).items.create(tweetJson);
};

AZURESQL.prototype.queryTweets = async function (keyword, numTweets) {
    const databaseId = "tweets";

    await this.client.database(databaseId).containers.createIfNotExists({ id: keyword });

    console.log(`Querying container: ${keyword}`);

    // query to return all children in a family
    const querySpec = {
        query: "SELECT * FROM c"
    };

    // Gets object from the db
    const { result: results } = await this.client.database(databaseId).container(keyword)
        .items.query(querySpec).toArray();

    if (results.length == 0) {
        return "no cache";
    }
    else {
        // Extracts the model from the first JSON object from the query result
        let tweets = [];
        for (let i = 0; i < results.length && i < numTweets; i++) {
            tweets.push(results[i]["text"]);
        }

        return tweets;
    }
};


module.exports = AZURESQL;