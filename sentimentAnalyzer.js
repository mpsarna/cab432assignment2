const bayes = require('bayes');


/*
* The class analyzes the sentiment for tweet as an interface that uses classifier model built by bayes library
*/
function ANALYZER(modelJson) {
    this.model = bayes.fromJson(modelJson);
}

ANALYZER.prototype.analyzeTweet = function (tweet) {
    let sentiment;
    const numRepeats = 5000;

    for (let i = 0; i < numRepeats; i++) {
        sentiment = this.model.categorize(tweet);
    }

    return sentiment;
}

ANALYZER.prototype.analyzeTweets = function (tweets) {
    let sentiments = [];
    for (let i = 0; i < tweets.length; i++) {
        sentiments.push(this.analyzeTweet(tweets[i]));
    }
    return sentiments;
}


module.exports = ANALYZER;