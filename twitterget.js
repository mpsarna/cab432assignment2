//Script for the Twitter API Function
module.exports = {

	gettwits: function(query, numTweets, callback) {
		// Require node module
		const Twit = require('twit');

		// Import Twitter Keys
        const config = require('./twitConfig');

		// Create Object
        let T = new Twit(config);

		// Search Params
        let params = {
			q: query,
			count: numTweets,
			lang: "en"
		};

		// Object function call to twitter
		T.get('search/tweets', params, gotData);

		// Function to output tweets
		function gotData(err, data){
			if (err) {
                callback('error');
			}
			else {
                let tweetContent = [];
                let tweets = data.statuses;

                //Extract the text data from the tweets
                for (let i in tweets) {
                    tweetContent.push(tweets[i].text);
                }

                callback(tweetContent);
            }
		};
		
	}
}

