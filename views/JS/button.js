console.log('button JS Running');

// event listener for button click
const button = document.getElementByID('searchbutton');

console.log(button);

button.addEventListener('click', function(e) {
	console.log('button was clicked');

	fetch('/clicked', {method: 'POST'})
    	.then(function(response) {
      		if(response.ok) {
        		console.log('Click was recorded');
        		return;
      		}
      		throw new Error('Request failed.');
    	})
    		.catch(function(error) {
      		console.log(error);
    });
});